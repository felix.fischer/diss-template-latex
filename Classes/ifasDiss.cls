%Tells the compiler which latex version is needed
\NeedsTeXFormat{LaTeX2e}

%Information about the class that will be Provided
\ProvidesClass{Classes/ifasDiss}[2023/07/05 ifas Dissertation Template]
\DeclareOption{english}{%
	\PassOptionsToPackage{ngerman, main=english}{babel}
}

\DeclareOption{ngerman}{%
	\PassOptionsToPackage{english, main=ngerman}{babel}
}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{extbook}}
\ProcessOptions

% encoding
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

%Language

\RequirePackage{babel}
% adds the translation command
\newcommand{\multilang}[2]{\iflanguage{german}{#1}{}\iflanguage{ngerman}{#1}{}\iflanguage{english}{#2}{}}

%Template Class and Font
\LoadClass[9pt, a5paper]{extbook}

\usepackage{csquotes}

\renewcommand{\baselinestretch}{1.15}

% sublime changes in spaces between symbols for improved aesthetics
\usepackage[%
    activate={true,nocompatibility},final,tracking=true,
    kerning=true,factor=1100,stretch=10,shrink=10]
{microtype}

% linebreaks in urls
\usepackage[hyphens]{url}
\usepackage{hyphenat}

% ignore v-space errors, which are small
\vbadness=3000

\usepackage{charter}

%Heading
\RequirePackage{titlesec}

%AMS Packages
\RequirePackage{amsfonts}
\RequirePackage{amsthm}
\RequirePackage{amsmath}
\RequirePackage{mathtools}

%Math: change space before and after equations 
\RequirePackage{xpatch}
\xapptocmd\normalsize{%
\abovedisplayskip=20pt plus 3pt minus 9pt
%\abovedisplayshortskip=0pt plus 3pt
\belowdisplayskip=20pt plus 3pt minus 9pt
%\belowdisplayshortskip=7pt plus 3pt minus 4pt
}{}{}

%Package to Modify Header Styles
\RequirePackage{titlesec}

%Package for tables
\RequirePackage{array}

%Package for Graphics
\usepackage{tikz}
\usepackage{tikz-3dplot}

%Multicolumn Package
\RequirePackage{multicol}

%Package to set Page size and margins
\RequirePackage[top=1.4cm, bottom=2cm, left=1.5cm, right=1.5cm, headsep=0.4cm]{geometry}

% package for units
\usepackage{siunitx}
\sisetup{display-per-mode = fraction, inline-per-mode = symbol}

% nicer last page margins
\usepackage{lastpage}

% multiline comments
\usepackage{verbatim}

% supress some warnings
\usepackage{silence} % suppress some wanrings
\WarningFilter{latex}{Underfull}
\WarningFilter{latex}{Overfull}
\WarningFilter{pdfTeX}{destination}

% add list of tables and such to TOC
\usepackage{tocbibind}

%Fancy header package to set header and footer for mainmatter
\RequirePackage{fancyhdr}
\fancypagestyle{acknowledgement}{%
	\renewcommand{\headrulewidth}{0.4pt}
	\fancyhf{}
        \cfoot{}
	%\fancyhead[LE]{\slshape Acknowledgement}
	\fancyhead[LE]{\slshape\leftmark}
	\fancyhead[RO]{\slshape\rightmark}
	\fancyfoot[LE,RO]{\thepage}
}

\fancypagestyle{mainmatter}{%
	\renewcommand{\headrulewidth}{0.4pt}
	\fancyhf{}
	\fancyhead[LE]{\slshape \leftmark}
	\fancyhead[RO]{\slshape \rightmark}
	\fancyfoot[LE,RO]{\thepage}
}

\fancypagestyle{backmatter}{%
	\renewcommand{\headrulewidth}{0.4pt}
	\fancyhf{}
	\fancyhead[LE]{\slshape \leftmark}
	\fancyhead[RO]{\slshape \rightmark}
	\fancyfoot[LE,RO]{\thepage}
}

%Set header and footer for chapter pages
\fancypagestyle{fancyplain}{%
	\renewcommand{\headrulewidth}{0pt}
	\fancyhf{}
	\fancyhead[RO]{\quad}
	\fancyfoot[RO]{\thepage}
}

%Commands for header marks
\renewcommand{\chaptermark}[1]{\markboth{\thechapter\hspace{2ex} #1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\hspace{2ex} #1}}

%Formatting--------------------------------------------------------------------------

%Format Chapter
\titleformat{\chapter}[block]
	{\thispagestyle{fancyplain}\huge\raggedright\bfseries\rmfamily%
	%\titlerule\vspace{0.8ex}
	}
	{\thechapter}{2ex}{}
	%[\titlerule]

%Format section
\titleformat{\section}[block]
	{\LARGE\raggedright\bfseries\rmfamily}
	{\thesection}{2ex}{}
	[]

%Format subsection	
\titleformat{\subsection}[block]
	{\Large\raggedright\bfseries\rmfamily}
	{\thesubsection}{2ex}{}
	[]

%Format subsubsection
\titleformat{\subsubsection}[block]
	{\large\raggedright\bfseries\rmfamily}
	{}{0ex}{}
	[]
	
%Spacing for chapter, section, subsection and subsubsection respectively
%\titlespacing*{\chapter}{0pt}{-6ex}{2ex}[0pt]
\titlespacing*{\chapter}{0pt}{-3ex plus 2ex minus 1ex}{%
    4ex plus 2ex minus 1ex}[0pt]
\titlespacing*{\section}{0pt}{3ex plus 3ex minus .5ex}{%
    2ex plus .1ex minus .5ex}[0ex]
\titlespacing*{\subsection}{0pt}{3ex plus 3ex minus .5ex}{%
    2ex plus .1ex minus .5ex}[0ex]
\titlespacing*{\subsubsection}{0pt}{2ex plus 2ex minus .5ex}{%
    1ex plus .1ex minus .2ex}[0ex]
%Environments for dedication, acknowledgement, and abstract respectively ------------
\newenvironment{dedication}{%
        \pagestyle{empty}
	\begin{multicols}{2} 
	\null\vfill\columnbreak
        \hfill\vspace{3cm}\hfill
	\begin{quote}}
{\end{quote}\end{multicols}\newpage\cleardoublepage }

\newenvironment{acknowledgement}
{%
    \pagestyle{acknowledgement}
    \chapter*{\multilang{Danksagungen}{Acknowledgements}}
    \markboth{\multilang{Danksagungen}{Acknowledgements}}{}
    \thispagestyle{empty}
    }
{%
	\setlength{\parindent}{0cm}
	\par
	\vspace{\baselineskip}
    Aachen, \today\hfill\@author
    \newpage
    \begingroup\pagestyle{empty}
    \cleardoublepage
    \endgroup
}

\newenvironment{zusammenfassung}
{%
    \pagestyle{acknowledgement}
    \chapter*{Zusammenfassung}
    \markboth{Zusammenfassung}{}
    \thispagestyle{empty}
    \begin{otherlanguage}{ngerman}}
{%
    \end{otherlanguage}
    \newpage
    \begingroup\pagestyle{empty}
    \cleardoublepage
    \endgroup
}

\newenvironment{abstract}
{%
    \pagestyle{acknowledgement}
    \chapter*{Abstract}
    \markboth{Abstract}{}
    \thispagestyle{empty}}
{%
    \newpage
    \begingroup\pagestyle{empty}
    \cleardoublepage
    \endgroup
}

\newenvironment{cv}
{\newpage\pagestyle{empty}
    \chapter*{\multilang{Lebenslauf}{Curriculum Vitae}}
	}
{%
    \newpage
    \begingroup\pagestyle{empty}
    \cleardoublepage
    \endgroup
}

% Nomenclature -------------------------------------------------------------------
%Packages
\RequirePackage[]{nomencl}
\RequirePackage{etoolbox, ragged2e}

\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}

\newcommand{\nomunit}[1]{%
    \renewcommand{\nomentryend}{%
        \noindent
        \parfillskip=0pt
        \parskip=0pt
        \hfill
        \begin{minipage}[t]{4em}
            #1
        \end{minipage}
        \par
    }
}

\newcommand{\UnitsCol}[1]{%
    \RaggedRight
    \hfill
    \parbox[t]{4em}{\RaggedRight #1}
    \ignorespaces
}

\newcommand{\nomsubtitle}[1]{\item[\large\bfseries #1]}

\renewcommand\nomgroup[1]{\def\nomtemp{\csname nomstart#1\endcsname}\nomtemp}

\newcommand{\nomstartR}{%
    \RaggedRight
    \vspace{1em}
    \nomsubtitle{\multilang{Lateinische Symbole}{Roman Symbols}}%
    \item[\bfseries Symbol]%
    \textbf{\multilang{Beschreibung}{Description}}
    \UnitsCol{\textbf{\multilang{Einheiten}{Units}}}
}

\newcommand{\nomstartG}{%
    \RaggedRight
    \vspace{1em}
    \nomsubtitle{\multilang{Griechische Symbole}{Greek Symbols}}%
    \item[\bfseries Symbol]%
    \textbf{\multilang{Beschreibung}{Description}}
    \UnitsCol{\textbf{\multilang{Einheiten}{Units}}}
}

\newcommand{\nomstartA}{%
    \RaggedRight
    \vspace{1em}
    \nomsubtitle{\multilang{Abkürzungen}{Acronyms}}%
    \item[\bfseries \multilang{Abkürzung}{Acronym}]
    \textbf{\multilang{Beschreibung}{Description}}
}

\renewcommand{\nomname}{\multilang{Nomenklatur}{Nomenclature}}
\renewcommand*{\nompreamble}{\markboth{\nomname}{\nomname}}


\newcommand{\nomtypeR}[4][]{\nomenclature[R#1]{#2}{#3\nomunit{#4}}}
\newcommand{\nomtypeG}[4][]{\nomenclature[G#1]{#2}{#3\nomunit{#4}}}
\newcommand{\nomtypeA}[3][]{\nomenclature[A#1]{#2}{#3}}

% renew command
\let\oldnomenclature\printnomenclature
\renewcommand{\printnomenclature}{%
    \pagestyle{acknowledgement}
    {%
        \thispagestyle{empty}
    	\hbadness=10000	
        \noindent
        \RaggedRight
        \parfillskip=0pt
        \parskip=0pt
        \pagestyle{acknowledgement}
        \oldnomenclature[6em]
        \addcontentsline{toc}{chapter}{\multilang{%
            Nomenklatur}{Nomenclature}
        }
    }
    \clearpage
    \pagestyle{empty}
    \thispagestyle{empty}
}
\makenomenclature


%Appendix---------------------------------------------------------------------
\usepackage{appendix}

%Packages for cv--------------------------------------------------------------
\usepackage{blindtext, scrextend}

%Figures--------------------------------------------------------------------------
\RequirePackage{graphicx,subcaption}

%Font for entire Document---------------------------------------------------------
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}

% Old Style-File inserted into this class file

\RequirePackage{wrapfig}

% hyperlinks
\usepackage[%
    hyperfootnotes=false, colorlinks=false,
    pdfborder={0 0 0}, pdfusetitle]{hyperref}


%Macros for Information about thesis
\def\firstCorrector#1{\gdef\@firstCorrector{#1}}
\def\secondCorrector#1{\gdef\@secondCorrector{#1}}
\def\dateOfExam#1{\gdef\@dateOfExam{#1}}
\def\doctorof#1{\gdef\@doctorof{#1}}

%Macro for Titlepage
\newcommand{\ltab}{\raggedright\arraybackslash} % Tabellenabschnitt linksbündig
\renewcommand{\maketitle}{%
    \pagenumbering{alph}
	\pagestyle{empty}
	\begin{center}
		\vspace{6pt}
		%\rule{\textwidth}{0.4pt}
                {\Huge \textbf{\@title} \par} %To have the title bold change with /bfseries
		%\rule[2mm]{\textwidth}{0.4pt}
	
		\vspace{50pt}
                \begin{otherlanguage}{ngerman}
		{Von der Fakultät für Maschinenwesen\\
		der Rheinisch-Westfälischen Technischen Hochschule Aachen\\
		zur Erlangung des akademischen Grades\\
		eines Doktors der \@doctorof \\
		genehmigte Dissertation}
	
		\vspace{72pt}
		vorgelegt von
	
		\vspace{20pt}
                {\Large \@author}
                \end{otherlanguage}
	\end{center}
	
	\vspace{156pt}
	\begin{flushleft}
	\begin{minipage}{0.2\textwidth}
		\hbadness=10000
		Berichter: \hfill\\
		\quad\hfill\\
	\end{minipage}
	\begin{minipage}{0.6\textwidth}
		\hbadness=10000
		\@firstCorrector \hfill\\
		\@secondCorrector \hfill \\
	\end{minipage}
	\\
           \begin{otherlanguage}{ngerman}
            \vspace{20pt}
			Tag der mündlichen Prüfung: \@dateOfExam
            \end{otherlanguage}
	\end{flushleft}
	\vfill
        \cleardoublepage
    \pagenumbering{roman}
}

%Math
\DeclareMathVersion{mathchartertext}
\SetSymbolFont{letters}{mathchartertext}{OML}{mdbch}{m}{n}
\newcommand{\charmu}{\mathversion{mathchartertext}$\mu$\mathversion{normal}}
% \charmu is used in text to type Greek "mu",
% resulting in Charter non-italic "mu"

%Spacing fraction
\newcommand{\myfrac}[3][0pt]{\genfrac{}{}{}{}{\raisebox{#1}{$#2$}}{\raisebox{-#1}{$#3$}}}

%Subscript
\newcommand{\s}[1]{\mathrm{#1}}

% nomitsep
\setlength{\nomitemsep}{-0.5ex}

\appto\frontmatter{%
    \pagenumbering{roman}
}

\appto\mainmatter{%
    \pagestyle{mainmatter}
    \pagenumbering{arabic}}

\appto\backmatter{%
    \pagestyle{backmatter}}

\appto\listoffigures{%
    \chaptermark{\listfigurename}
}

\appto\listoftables{%
        \chaptermark{\listtablename}
}

\renewcommand\tableofcontents{%
    \if@twocolumn
      \@restonecoltrue\onecolumn
    \else
      \@restonecolfalse
    \fi
    \pagestyle{acknowledgement}
    \chapter*{\multilang{Inhaltsverzeichnis}{Table of Contents}}%
    \thispagestyle{empty}
    \markboth{\multilang{Inhaltsverzeichnis}{Table of Contents}}{%
        \multilang{Inhaltsverzeichnis}{Table of Contents}}
    \@starttoc{toc}
    \if@restonecol\twocolumn\fi
    \clearpage
    \pagestyle{empty}
    \thispagestyle{empty}
}

% subliminal parskip
\parskip=0pt plus 1pt