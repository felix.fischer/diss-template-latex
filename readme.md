# ifas LaTeX Template for Disserations
The ifas LaTeX-template for dissertations.

## Table of Content

[[_TOC_]]


## Overview

This LaTeX-template can be used to create dissertations.
The end result should look similar to other theses written at ifas.
It is based on the template by Marco Longhitano and Stefan Jeske.

## Usage
The file *thesis.tex* can be compiled directly by texstudio or overleaf.
The arara commands on top of *thesis.tex* can be used to compile the document
directly from command line:
```
arara dissertation.tex
```

All necessary information for the creation of the title, like the name of the
author, must be entered in *dissertation.tex*.

- Use in Overleaf:
    * Create a new project and click on upload project
    * Upload this repo as a zip file. You can download it here directly


### Nomenclature
The creation of the nomenclature is handled automatically by Arara and
Overleaf. In case of Texstudio addtional steps are necessary:
* Go to *Options* -> *Build* -> *User Commands*
* Enter into the left field `user0:Make Nomenclaure`
* Enter into the right field `makeindex %.nlo -s nomencl.ist -o %.nls -t %.nlg
* Press enter
Now it is possible to press *Alt+Shift+F1* to generate or actualize the
nomenclature between compilations.


## Troubleshooting

Common errors and their solution are found in the [Wiki](https://git.ifas.rwth-aachen.de/templates/ifas-latex-template/-/wikis/Troubleshooting)
